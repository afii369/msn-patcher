To run, run `run.bat` with "Run as administrator".
If you still get an error about permissions, right click `version` > Properties and uncheck "Read-only".

`msi2xml` is from http://msi2xml.sourceforge.net.

`msidcrl.dll` is built from https://gitlab.com/valtron/msn-msidcrl.
